plugins {
  java
  jacoco
  id("org.springframework.boot") version "3.2.2"
  id("io.spring.dependency-management") version "1.1.0"
  id("checkstyle")
}

dependencyManagement {
  imports {
    mavenBom("org.springframework.cloud:spring-cloud-dependencies:2022.0.4")
  }
}

group = "com.acme"

repositories {
  mavenCentral()
}

val mapStructBindingVersion = "0.2.0"
val mapStructVersion = "1.5.5.Final"
val brainsVersion = "24.0.1"
val jsonPathVersion = "2.8.0"
val openapiVersion = "2.3.0"
val oauthClientVersion = "3.2.2"

dependencies {
  implementation("org.springframework.boot:spring-boot-starter-actuator")
  implementation("org.springframework.boot:spring-boot-starter-web")
  implementation("org.springframework.boot:spring-boot-starter-data-jpa")
  implementation("org.springframework.boot:spring-boot-starter-validation")
  implementation("org.springframework.boot:spring-boot-starter-security")
  implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
  implementation("org.mapstruct:mapstruct:$mapStructVersion")
  implementation("org.liquibase:liquibase-core")
  implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:$openapiVersion")

  compileOnly("org.jetbrains:annotations:$brainsVersion")
  compileOnly("org.projectlombok:lombok")

  runtimeOnly("org.postgresql:postgresql")

  annotationProcessor("org.projectlombok:lombok-mapstruct-binding:$mapStructBindingVersion")
  annotationProcessor("org.mapstruct:mapstruct-processor:$mapStructVersion")
  annotationProcessor("org.projectlombok:lombok")
  annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")

  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testCompileOnly("org.projectlombok:lombok")
  testAnnotationProcessor("org.projectlombok:lombok")
}

val testExclusions = listOf(
  "**/configuration/**",
  "**/model/**",
  "**/enums/**",
  "**/exceptions/**",
  "**/TodoApplication*",
)

jacoco {
  toolVersion = "0.8.9"
}

tasks {

  withType<JavaCompile> {
    sourceCompatibility = "${JavaVersion.VERSION_17}"
    targetCompatibility = "${JavaVersion.VERSION_17}"
  }

  getByName<JacocoReport>("jacocoTestReport") {
    dependsOn(findByName("check"))
    executionData.setFrom(
      fileTree(layout.buildDirectory).include("/jacoco/*.exec")
    )
    afterEvaluate {
      classDirectories.setFrom(
        files(
          classDirectories
            .files
            .map {
              fileTree(it) {
                exclude(testExclusions)
              }
            }
        )
      )
    }
    reports {
      xml.required.set(true)
      xml.outputLocation.set(file("${projectDir.path}/build/reports/jacoco/test/jacocoTestReport.xml"))
    }
  }

  checkstyle {
    configFile = file("checkstyle.xml")
  }

  checkstyleMain {
    maxWarnings = 0
    dependsOn(findByName("classes"))
  }

  test {
    useJUnitPlatform()
    testLogging {
      showExceptions = true
      showStandardStreams = false
      showCauses = true
      exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    }
  }

  checkstyleTest {
    enabled = false
  }

  jar {
    enabled = false
  }

}
