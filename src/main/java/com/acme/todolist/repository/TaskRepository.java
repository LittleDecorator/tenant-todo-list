package com.acme.todolist.repository;

import com.acme.todolist.model.entity.TaskEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<TaskEntity, Long> {

  List<TaskEntity> findAllByUsername(String username);

  boolean existsByIdAndUsername(Long id, String username);

  TaskEntity getReferenceByIdAndUsername(Long id, String username);

}
