package com.acme.todolist.handler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.ValidationException;
import jakarta.validation.constraints.NotNull;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  @Override
  protected @NotNull ResponseEntity<Object> handleMethodArgumentNotValid(
    MethodArgumentNotValidException ex,
    @NotNull HttpHeaders headers,
    @NotNull HttpStatusCode status,
    @NotNull WebRequest request
  ) {
    var apiError = new ApiError(BAD_REQUEST);
    apiError.setMessage("Validation error");
    apiError.addValidationErrors(ex.getBindingResult().getFieldErrors());
    apiError.addValidationError(ex.getBindingResult().getGlobalErrors());
    return buildResponseEntity(apiError);
  }

  @ExceptionHandler(ConstraintViolationException.class)
  protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex) {
    logger.error("Constraint validations failed!", ex);
    var apiError = new ApiError(BAD_REQUEST);
    apiError.setMessage("Validation error");
    apiError.addValidationErrors(ex.getConstraintViolations());
    return buildResponseEntity(apiError);
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(
    MethodArgumentTypeMismatchException ex) {
    var apiError = new ApiError(BAD_REQUEST);
    apiError.setMessage(
      MessageFormatter.arrayFormat(
        "Invalid parameter value is passed to the request: {}. "
          + "The parameter {} of value {} could not be converted to {}",
        new Object[]{
          ex.getValue(),
          ex.getName(),
          ex.getValue(),
          Optional.ofNullable(ex.getRequiredType()).map(Class::getSimpleName).orElse("desired type")
        }
      ).getMessage());
    apiError.setDetail(ex.getMessage());
    return buildResponseEntity(apiError);
  }

  @ExceptionHandler({ValidationException.class})
  public ResponseEntity<Object> handleValidationException(ValidationException ex) {
    logger.error("Validation checks failed!", ex);
    return buildResponseEntity(BAD_REQUEST, ex);
  }

  @ExceptionHandler({IllegalArgumentException.class})
  public ResponseEntity<Object> handleArgumentExceptions(IllegalArgumentException ex) {
    return buildResponseEntity(BAD_REQUEST, ex);
  }

  @ExceptionHandler({Exception.class})
  public ResponseEntity<Object> handleOtherExceptions(Exception ex) {
    logger.error("Successfully handled Exception!", ex);
    return buildResponseEntity(INTERNAL_SERVER_ERROR, ex);
  }

  @ExceptionHandler({EntityNotFoundException.class})
  public ResponseEntity<Object> handleReourceNotFoundException(EntityNotFoundException ex) {
    return buildResponseEntity(NOT_FOUND, ex);
  }

  private ResponseEntity<Object> buildResponseEntity(HttpStatus status, Exception ex) {
    var cause = Optional.ofNullable(ex.getCause()).map(Throwable::getMessage).orElse(null);
    var apiError = new ApiError(status)
      .setMessage(ex.getMessage())
      .setDetail(cause);
    return buildResponseEntity(apiError);
  }

  private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
    return new ResponseEntity<>(apiError, apiError.getStatus());
  }
}
