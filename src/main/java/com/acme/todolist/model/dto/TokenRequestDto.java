package com.acme.todolist.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;

@Schema
public record TokenRequestDto(
  @Schema(example = "mike@other.com")
  @NotNull String username,

  @Schema(example = "mike123")
  @NotNull String password,

  @Schema(hidden = true)
  String clientId,

  @Schema(hidden = true)
  String clientSecret
) {

  public TokenRequestDto {
    clientId = StringUtils.defaultIfBlank(clientId, "newClient");
    clientSecret = StringUtils.defaultIfBlank(clientSecret, "newClientSecret");
  }

}
