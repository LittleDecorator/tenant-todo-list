package com.acme.todolist.model.dto;

public record TaskDto(
  Long id,
  String title,
  String description
) {}
