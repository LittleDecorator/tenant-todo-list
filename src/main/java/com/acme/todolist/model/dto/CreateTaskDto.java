package com.acme.todolist.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;

@Schema
public record CreateTaskDto(
  @Schema(example = "Basic title")
  @NotNull String title,

  @Schema(example = "Simple dummy description")
  String description
) {
}
