package com.acme.todolist.model.dto;

public record UpdateTaskDto(String title, String description) {
}
