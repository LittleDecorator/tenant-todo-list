package com.acme.todolist.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public record TokenResponseDto(@JsonProperty("access_token") String accessToken) {
}
