package com.acme.todolist.model.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "todo_items")
public class TaskEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "todo_items_gen")
  @SequenceGenerator(name = "todo_items_gen", sequenceName = "todo_items_id_seq", allocationSize = 1)
  Long id;
  String title;
  String description;
  String username;

}
