package com.acme.todolist.controller;

import com.acme.todolist.model.dto.TokenRequestDto;
import com.acme.todolist.model.dto.TokenResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Token", description = "Token management APIs")
public interface TokenController {

  @Operation(summary = "Getting access token")
  @ApiResponses({
    @ApiResponse(
      responseCode = "200",
      description = "Token successfully requested",
      content = {
        @Content(schema = @Schema(implementation = TokenResponseDto.class), mediaType = "application/json")
      }),
    @ApiResponse(responseCode = "500", description = "Internal service error", content = {@Content(schema = @Schema())})
  })
  TokenResponseDto getAccessToken(@RequestBody TokenRequestDto requestDto);
}
