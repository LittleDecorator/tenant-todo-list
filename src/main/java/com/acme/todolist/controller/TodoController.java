package com.acme.todolist.controller;

import com.acme.todolist.model.dto.CreateTaskDto;
import com.acme.todolist.model.dto.TaskDto;
import com.acme.todolist.model.dto.UpdateTaskDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.security.core.context.SecurityContext;


@Tag(name = "Tasks", description = "Tasks management APIs")
@SecurityRequirement(name = "bearerAuth")
public interface TodoController {

  @Operation(summary = "Getting tasks", description = "Collecting available TODO tasks")
  @ApiResponses({
    @ApiResponse(
      responseCode = "200",
      description = "Available task collection received",
      content = {
        @Content(
          mediaType = "application/json",
          array = @ArraySchema(schema = @Schema(implementation = TaskDto.class)))
      }),
    @ApiResponse(responseCode = "500", description = "Internal service error", content = {@Content(schema = @Schema())})
  })
  List<TaskDto> getTasks(@Parameter(hidden = true) SecurityContext context);

  @Operation(
    summary = "Create new task",
    description = "Just create a new task"
  )
  @ApiResponses({
    @ApiResponse(
      responseCode = "200",
      description = "Tasks successfully created",
      content = {
        @Content(schema = @Schema(implementation = TaskDto.class), mediaType = "application/json")
      }),
    @ApiResponse(responseCode = "400", description = "Incorrect request parameter are specified", content = {@Content(schema = @Schema())}),
    @ApiResponse(responseCode = "500", description = "Internal service error", content = {@Content(schema = @Schema())})
  })
  TaskDto addTask(@RequestBody CreateTaskDto dto, @Parameter(hidden = true) SecurityContext context);

  @Operation(
    summary = "Delete task",
    description = "Removing existed task by it's ID"
  )
  @ApiResponses({
    @ApiResponse(responseCode = "200", description = "Task successfully removed"),
    @ApiResponse(responseCode = "404", description = "Required resource not found"),
    @ApiResponse(responseCode = "500", description = "Internal service error")
  })
  void deleteTask(
    @Parameter(name = "taskId", description = "Task identifier", example = "1", required = true) long taskId,
    @Parameter(hidden = true) SecurityContext context
  );

  @Operation(
    summary = "Update task",
    description = "Updating already existed task"
  )
  @ApiResponses({
    @ApiResponse(responseCode = "200", description = "Tasks successfully modified"),
    @ApiResponse(responseCode = "404", description = "Required resource not found"),
    @ApiResponse(responseCode = "500", description = "Internal service error")
  })
  void updateTask(
    @Parameter(name = "taskId", description = "Task identifier", example = "1", required = true) long taskId,
    @RequestBody UpdateTaskDto dto,
    @Parameter(hidden = true) SecurityContext context
  );
}
