package com.acme.todolist.controller.impl;

import com.acme.todolist.controller.TokenController;
import com.acme.todolist.model.dto.TokenRequestDto;
import com.acme.todolist.model.dto.TokenResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClient;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/token")
public class TokenControllerImpl implements TokenController {

  @Value("${spring.security.oauth2.token.url}")
  private String keycloakUrl;

  @GetMapping
  public TokenResponseDto getAccessToken(TokenRequestDto dto) {
    log.info("Taking new assess token");

    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("username", dto.username());
    map.add("password", dto.password());
    map.add("client_id", dto.clientId());
    map.add("client_secret", dto.clientSecret());
    map.add("grant_type", "password");

    return RestClient.create(keycloakUrl)
      .post()
      .contentType(MediaType.APPLICATION_FORM_URLENCODED)
      .body(map)
      .retrieve()
      .toEntity(TokenResponseDto.class)
      .getBody();
  }

}
