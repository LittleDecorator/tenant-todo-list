package com.acme.todolist.controller.impl;

import com.acme.todolist.controller.TodoController;
import com.acme.todolist.mapper.TaskMapper;
import com.acme.todolist.model.dto.CreateTaskDto;
import com.acme.todolist.model.dto.TaskDto;
import com.acme.todolist.model.dto.UpdateTaskDto;
import com.acme.todolist.service.TaskService;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/tasks")
public class TodoControllerImpl implements TodoController {

  private final TaskService taskService;
  private final TaskMapper taskMapper;

  @GetMapping
  public List<TaskDto> getTasks(@CurrentSecurityContext SecurityContext context) {
    log.info("Collecting todo tasks");
    return taskMapper.toDto(taskService.findTasks(context.getAuthentication().getName()));
  }

  @PostMapping
  public TaskDto addTask(@Valid @RequestBody CreateTaskDto dto, @CurrentSecurityContext SecurityContext context) {
    log.info("Adding todo task [{}]", dto.title());
    return taskMapper.toDto(taskService.createTask(taskMapper.toModel(dto), context.getAuthentication().getName()));
  }

  @DeleteMapping("/{taskId}")
  public void deleteTask(@PathVariable("taskId") long taskId, @CurrentSecurityContext SecurityContext context) {
    log.info("Delete todo task with id [{}]", taskId);
    taskService.removeTask(taskId, context.getAuthentication().getName());
  }

  @PutMapping("/{taskId}")
  public void updateTask(@PathVariable("taskId") long taskId, @RequestBody UpdateTaskDto dto, @CurrentSecurityContext SecurityContext context) {
    log.info("Updating todo task with id [{}]", taskId);
    taskService.updateTask(taskMapper.toModel(dto, taskId), context.getAuthentication().getName());
  }

}
