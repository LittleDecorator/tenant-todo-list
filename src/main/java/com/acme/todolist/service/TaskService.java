package com.acme.todolist.service;

import com.acme.todolist.model.Task;
import java.util.List;

public interface TaskService {

  List<Task> findTasks(String username);

  Task createTask(Task task, String username);

  void updateTask(Task task, String username);

  void removeTask(long taskId, String username);

}
