package com.acme.todolist.service.impl;

import com.acme.todolist.exception.TaskNotFoundException;
import com.acme.todolist.mapper.TaskMapper;
import com.acme.todolist.model.Task;
import com.acme.todolist.repository.TaskRepository;
import com.acme.todolist.service.TaskService;
import jakarta.transaction.Transactional;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

  private final TaskRepository repository;
  private final TaskMapper mapper;

  @Override
  public List<Task> findTasks(String username) {
    log.info("Collect all todo tasks from database");
    return mapper.toModel(repository.findAllByUsername(username));
  }

  @Override
  public Task createTask(Task task, String username) {
    log.info("Creating new task {}", task.getTitle());
    var entity = mapper.toEntity(task).setUsername(username);
    return mapper.toModel(
      repository.save(entity)
    );
  }

  @Override
  @Transactional
  public void updateTask(Task task, String username) {
    Assert.notNull(task.getId(), "The ID is required for task updating");
    var target = repository.getReferenceByIdAndUsername(task.getId(), username);
    mapper.update(task, target);
  }

  @Override
  public void removeTask(long taskId, String username) {
    if (!repository.existsByIdAndUsername(taskId, username)) {
      throw new TaskNotFoundException("TODO task with [" + taskId + "] does not exists or belongs to [" + username + "]");
    }
    repository.deleteById(taskId);
  }

}
