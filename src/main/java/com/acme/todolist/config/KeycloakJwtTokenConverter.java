package com.acme.todolist.config;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimNames;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

@Configuration
@RequiredArgsConstructor
public class KeycloakJwtTokenConverter implements Converter<Jwt, JwtAuthenticationToken> {

  private static final String RESOURCE_ACCESS = "resource_access";
  private static final String ROLES = "roles";
  private static final String ROLE_PREFIX = "ROLE_";

  private final JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
  private final TokenConverterProperties properties;

  @Override
  @SuppressWarnings("unchecked")
  public JwtAuthenticationToken convert(@NotNull Jwt jwt) {
    Stream<SimpleGrantedAuthority> accesses = Optional.of(jwt)
      .map(token -> token.getClaimAsMap(RESOURCE_ACCESS))
      .map(claimMap -> (Map<String, Object>) claimMap.get(properties.getResourceId()))
      .map(resourceData -> (Collection<String>) resourceData.get(ROLES))
      .stream()
      .map(role -> new SimpleGrantedAuthority(ROLE_PREFIX + role))
      .distinct();

    Set<GrantedAuthority> authorities = Stream
      .concat(jwtGrantedAuthoritiesConverter.convert(jwt).stream(), accesses)
      .collect(Collectors.toSet());

    String principalClaimName = properties.getPrincipalAttribute()
      .map(jwt::getClaimAsString)
      .orElse(jwt.getClaimAsString(JwtClaimNames.SUB));

    return new JwtAuthenticationToken(jwt, authorities, principalClaimName);
  }
}
