package com.acme.todolist.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

  public static final String[] SWAGGER_PATTERNS = {
    "v3/api-docs/**",
    "/**.html", "/**.js", "/**.js.map", "/**.yml", "/*.woff2", "/**.css", "/*.ico",
    "swagger-ui/*.html", "swagger-ui/*.css", "swagger-ui/*.js", "swagger-ui/*.png"
  };

  @Bean
  public OpenAPI specification() {
    Server devServer = new Server();
    devServer.setUrl("http://localhost:7878");
    devServer.setDescription("Server URL in Development environment");

    License mitLicense = new License().name("MIT License").url("https://choosealicense.com/licenses/mit/");

    Info info = new Info()
      .title("TodoList Service - OpenAPI 3.0")
      .version("0.1.0")
      .description("Service manage your TODO tasks")
      .license(mitLicense);

    final String securitySchemeName = "bearerAuth";

    return new OpenAPI()
      .components(
        new Components()
          .addSecuritySchemes(securitySchemeName,
            new SecurityScheme()
              .name(securitySchemeName)
              .type(SecurityScheme.Type.HTTP)
              .scheme("bearer")
              .bearerFormat("JWT")
          )
      )
      .info(info).addServersItem(devServer);
  }

}
