package com.acme.todolist.config;

import java.util.Optional;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Configuration
@ConfigurationProperties(prefix = "spring.security.oauth2.token.converter")
public class TokenConverterProperties {

  @Getter
  private String resourceId;
  private String principalAttribute;

  public Optional<String> getPrincipalAttribute() {
    return Optional.ofNullable(principalAttribute);
  }

}
