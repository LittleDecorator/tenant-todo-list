package com.acme.todolist.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableMethodSecurity
@RequiredArgsConstructor
public class WebSecurityConfiguration {

  private final KeycloakJwtTokenConverter tokenConverter;

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http
      .authorizeHttpRequests(auth -> auth
        .requestMatchers("/api/v1/token/**").permitAll()
        .requestMatchers(SwaggerConfig.SWAGGER_PATTERNS).permitAll()
        .anyRequest().authenticated()
      )
      .oauth2ResourceServer(server -> server.jwt(jwtConfigurer -> jwtConfigurer.jwtAuthenticationConverter(tokenConverter)))
      .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
    return http.build();

  }

}
