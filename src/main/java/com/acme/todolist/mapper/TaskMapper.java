package com.acme.todolist.mapper;

import com.acme.todolist.model.Task;
import com.acme.todolist.model.dto.CreateTaskDto;
import com.acme.todolist.model.dto.TaskDto;
import com.acme.todolist.model.dto.UpdateTaskDto;
import com.acme.todolist.model.entity.TaskEntity;
import java.util.Collection;
import java.util.List;
import org.mapstruct.BeanMapping;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = ComponentModel.SPRING, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface TaskMapper {

  @Mapping(target = "id", ignore = true)
  Task toModel(CreateTaskDto dto);

  @Mapping(target = "id", source = "taskId")
  Task toModel(UpdateTaskDto dto, Long taskId);

  Task toModel(TaskEntity entity);

  @IterableMapping(elementTargetType = Task.class)
  List<Task> toModel(Collection<TaskEntity> entity);

  TaskDto toDto(Task model);

  @IterableMapping(elementTargetType = TaskDto.class)
  List<TaskDto> toDto(Collection<Task> model);

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "username", ignore = true)
  TaskEntity toEntity(Task model);

  @Mapping(target = "id", ignore = true)
  @BeanMapping(
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    unmappedTargetPolicy = ReportingPolicy.IGNORE
  )
  void update(Task source, @MappingTarget TaskEntity target);
}
