create table todo_items
(
  id              bigserial   not null constraint todo_pk primary key,
  title           varchar     not null,
  description     varchar,
  username        varchar     not null
);
