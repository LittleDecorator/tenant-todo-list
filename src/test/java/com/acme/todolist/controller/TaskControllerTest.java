package com.acme.todolist.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.acme.todolist.config.KeycloakJwtTokenConverter;
import com.acme.todolist.mapper.TaskMapper;
import com.acme.todolist.mapper.TaskMapperImpl;
import com.acme.todolist.model.Task;
import com.acme.todolist.service.TaskService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.test.context.event.RecordApplicationEvents;
import org.springframework.test.web.servlet.MockMvc;

@RecordApplicationEvents
@WebMvcTest({
  TodoController.class,
  TaskMapperImpl.class,
})
@MockBean({
  TaskService.class,
  KeycloakJwtTokenConverter.class
})
public class TaskControllerTest {

  @Autowired
  MockMvc mvc;
  @Autowired
  TaskMapper taskMapper;
  @Autowired
  ObjectMapper objectMapper;

  @MockBean
  TaskService taskService;

  @Captor
  ArgumentCaptor<Task> taskArgumentCaptor;

  @Test
  @DisplayName("Respond 200, when collecting all tasks")
  @SneakyThrows
  void successOnGettingTasks() {
    // GIVEN
    var task = new Task(1L, "Test title", "Dummy test content");

    // WHEN
    doReturn(List.of(task)).when(taskService).findTasks(anyString());

    // ACT
    mvc.perform(
        get("/api/v1/tasks")
          .contentType(MediaType.APPLICATION_JSON_VALUE)
      )
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[0].title", is(task.getTitle())));
  }

  @Test
  @DisplayName("Respond 200, on successful task update request")
  @SneakyThrows
  void successOnTaskUpdating() {
    // ACT
    mvc.perform(
        put("/api/v1/tasks/{taskId}", 1)
          .contentType(MediaType.APPLICATION_JSON_VALUE)
          .content("{ \"description\": \"new description content\" }")
      )
      .andExpect(status().isOk());

    // THEN
    verify(taskService).updateTask(taskArgumentCaptor.capture(), anyString());
    assertEquals(1, taskArgumentCaptor.getValue().getId());
  }

  @Test
  @DisplayName("Respond 400, if create task without `title`")
  @SneakyThrows
  void errorOnCreatingApplicationWithWrongSpec() {
    // ACT
    mvc.perform(
        post("/api/v1/tasks")
          .contentType(MediaType.APPLICATION_JSON_VALUE)
          .content("{ \"description\": \"sample content\" }")
      )
      .andExpect(status().isBadRequest())
      .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
      .andExpect(jsonPath("$.errors[0].field", is("title")))
      .andExpect(jsonPath("$.errors[0].message", is("must not be null")))
      .andDo(print());

    // THEN
    verifyNoInteractions(taskService);
  }

  @TestConfiguration
  public static class UpdaterProvider {

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
      return http
        .csrf(AbstractHttpConfigurer::disable)
        .authorizeHttpRequests(auth -> auth.anyRequest().permitAll())
        .build();
    }

  }
}
