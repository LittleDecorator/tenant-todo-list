FROM eclipse-temurin:17-jre-alpine
EXPOSE 7878
ARG JAR_FILE=build/libs/tenant-todo-list.jar
ADD ${JAR_FILE} /tenant-todo-list.jar
ENTRYPOINT ["java","-Xmx4g","-Xms2g","-XX:+UseContainerSupport","-Djava.security.egd=file:/dev/./urandom","-jar","/tenant-todo-list.jar"]
